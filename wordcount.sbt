name := "word-count"

version := "1.0.0"

scalaVersion := "2.12.8"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.4.0" % "provided"
libraryDependencies += "org.datanucleus" % "datanucleus-api-jdo" % "5.1.9" % "provided"
libraryDependencies += "org.datanucleus" % "datanucleus-core" % "5.1.9" % "provided"
libraryDependencies += "org.datanucleus" % "datanucleus-rdbms" % "5.1.9" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-network-yarn" % "2.4.0" % "provided"
